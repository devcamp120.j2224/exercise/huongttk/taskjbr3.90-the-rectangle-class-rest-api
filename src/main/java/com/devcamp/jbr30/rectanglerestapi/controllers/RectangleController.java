package com.devcamp.jbr30.rectanglerestapi.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.jbr30.rectanglerestapi.models.Rectangle;

@RestController
public class RectangleController {
    @CrossOrigin
    @GetMapping("/rectangle-area")
    public double getArea(@RequestParam(value="width", required=true) double width, @RequestParam(value="height", required=true) double height){
        Rectangle h1 = new Rectangle(width, height);
        double area = h1.getArea();
        return area;
    }
    @GetMapping("/rectangle-perimeter")
    public double getPerimeter(@RequestParam(value="width", required=true) double width, @RequestParam(value="height", required=true) double height){
        Rectangle h1 = new Rectangle(width, height);
        double perimeter = h1.getPerimeter();
        return perimeter;
    }
    
}
